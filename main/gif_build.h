#ifndef _GIF_TEST_H_
#define _GIF_TEST_H_

/// Format of the input buffer to encode as a gif image
enum gif_source_format {
	/// RGB, 8 bits per channel, one buffer per channel
	RGB888_BUFS,
	/// RGB, 8 bits per channel, interleaved RGB samples
	RGB888_INTERLEAVED,
	/// RGB, 5/6/5 bits per channel, interleaved RGB samples
	/// (16 bits per pixel)
	RGB565_INTERLEAVED,
};

/// Callback type, executed when there is new encoded gif data
typedef int (*out_data_func)(void *context, const char *encoded, int length);

/************************************************************************//**
 * \brief Initialize the generation of an animated gif.
 *
 * Call this function before adding any data with gif_add().
 *
 * \param[in] width     Width in pixels of the generated gif.
 * \param[in] height    Height in pixels of the generated gif.
 * \param[in] user_data Optional user data to pass to data_cb when called.
 * \param[in] data_cb   Callback function to consume output data.
 *
 * \return 0 on success, non-zero if error.
 ****************************************************************************/
int gif_init(int width, int height, void *user_data, out_data_func data_cb);

/************************************************************************//**
 * \brief Add a frame to an animated gif.
 *
 * Call this function one or more times after a successful call to gif_init().
 *
 * \param[in] buf      Buffer with the frame to add.
 * \param[in] delay_cs Delay in tenths of milliseconds for the frame (e.g.
 *            set it to 100 for a 1 second delay).
 * \param[in] format   Pixel format for the input buf.
 *
 * \return 0 on success, non-zero if error.
 ****************************************************************************/
int gif_add(const char *buf, int delay_cs, enum gif_source_format format);

/************************************************************************//**
 * \brief Finish the generation of the gif file.
 *
 * Call this function after one or more successful gif_add() calls.
 *
 * \return 0 on success, non-zero if error.
 ****************************************************************************/
int gif_close(void);

/************************************************************************//**
 * \brief Generates a test gif image.
 *
 * \param[in] user_data Optional user data to pass to data_cb when called.
 * \param[in] data_cb   Callback function to consume output data.
 * \param[in] width     Width in pixels of the generated gif.
 * \param[in] height    Height in pixels of the generated gif.
 *
 * \return 0 on success, non-zero if error.
 ****************************************************************************/
int gif_generate_test(void *user_data, out_data_func data_cb,
		int width, int height);

#endif /*_GIF_TEST_H_*/

