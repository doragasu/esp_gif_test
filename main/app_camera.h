/*
  * ESPRESSIF MIT License
  *
  * Copyright (c) 2017 <ESPRESSIF SYSTEMS (SHANGHAI) PTE LTD>
  *
  * Permission is hereby granted for use on ESPRESSIF SYSTEMS products only, in which case,
  * it is free of charge, to any person obtaining a copy of this software and associated
  * documentation files (the "Software"), to deal in the Software without restriction, including
  * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
  * and/or sell copies of the Software, and to permit persons to whom the Software is furnished
  * to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all copies or
  * substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  *
  */
#ifndef _APP_CAMERA_H_
#define _APP_CAMERA_H_

#include <esp_camera.h>

#if CONFIG_CAMERA_MODEL_WROVER_KIT
#define PWDN_GPIO_NUM    -1
#define RESET_GPIO_NUM   -1
#define XCLK_GPIO_NUM    21
#define SIOD_GPIO_NUM    26
#define SIOC_GPIO_NUM    27

#define Y9_GPIO_NUM      35
#define Y8_GPIO_NUM      34
#define Y7_GPIO_NUM      39
#define Y6_GPIO_NUM      36
#define Y5_GPIO_NUM      19
#define Y4_GPIO_NUM      18
#define Y3_GPIO_NUM       5
#define Y2_GPIO_NUM       4
#define VSYNC_GPIO_NUM   25
#define HREF_GPIO_NUM    23
#define PCLK_GPIO_NUM    22

#elif CONFIG_CAMERA_MODEL_ESP_EYE
#define PWDN_GPIO_NUM    -1
#define RESET_GPIO_NUM   -1
#define XCLK_GPIO_NUM    4
#define SIOD_GPIO_NUM    18
#define SIOC_GPIO_NUM    23

#define Y9_GPIO_NUM      36
#define Y8_GPIO_NUM      37
#define Y7_GPIO_NUM      38
#define Y6_GPIO_NUM      39
#define Y5_GPIO_NUM      35
#define Y4_GPIO_NUM      14
#define Y3_GPIO_NUM      13
#define Y2_GPIO_NUM      34
#define VSYNC_GPIO_NUM   5
#define HREF_GPIO_NUM    27
#define PCLK_GPIO_NUM    25

#elif CONFIG_CAMERA_MODEL_M5STACK_PSRAM
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    15
#define XCLK_GPIO_NUM     27
#define SIOD_GPIO_NUM     25
#define SIOC_GPIO_NUM     23

#define Y9_GPIO_NUM       19
#define Y8_GPIO_NUM       36
#define Y7_GPIO_NUM       18
#define Y6_GPIO_NUM       39
#define Y5_GPIO_NUM        5
#define Y4_GPIO_NUM       34
#define Y3_GPIO_NUM       35
#define Y2_GPIO_NUM       32
#define VSYNC_GPIO_NUM    22
#define HREF_GPIO_NUM     26
#define PCLK_GPIO_NUM     21

#elif CONFIG_CAMERA_MODEL_M5STACK_WIDE
#define PWDN_GPIO_NUM     -1
#define RESET_GPIO_NUM    15
#define XCLK_GPIO_NUM     27
#define SIOD_GPIO_NUM     22
#define SIOC_GPIO_NUM     23

#define Y9_GPIO_NUM       19
#define Y8_GPIO_NUM       36
#define Y7_GPIO_NUM       18
#define Y6_GPIO_NUM       39
#define Y5_GPIO_NUM        5
#define Y4_GPIO_NUM       34
#define Y3_GPIO_NUM       35
#define Y2_GPIO_NUM       32
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     26
#define PCLK_GPIO_NUM     21

#elif CONFIG_CAMERA_MODEL_AI_THINKER
#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27

#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22


#elif CONFIG_CAMERA_MODEL_CUSTOM
#define PWDN_GPIO_NUM    CONFIG_CAMERA_PIN_PWDN
#define RESET_GPIO_NUM   CONFIG_CAMERA_PIN_RESET
#define XCLK_GPIO_NUM    CONFIG_CAMERA_PIN_XCLK
#define SIOD_GPIO_NUM    CONFIG_CAMERA_PIN_SIOD
#define SIOC_GPIO_NUM    CONFIG_CAMERA_PIN_SIOC

#define Y9_GPIO_NUM      CONFIG_CAMERA_PIN_Y9
#define Y8_GPIO_NUM      CONFIG_CAMERA_PIN_Y8
#define Y7_GPIO_NUM      CONFIG_CAMERA_PIN_Y7
#define Y6_GPIO_NUM      CONFIG_CAMERA_PIN_Y6
#define Y5_GPIO_NUM      CONFIG_CAMERA_PIN_Y5
#define Y4_GPIO_NUM      CONFIG_CAMERA_PIN_Y4
#define Y3_GPIO_NUM      CONFIG_CAMERA_PIN_Y3
#define Y2_GPIO_NUM      CONFIG_CAMERA_PIN_Y2
#define VSYNC_GPIO_NUM   CONFIG_CAMERA_PIN_VSYNC
#define HREF_GPIO_NUM    CONFIG_CAMERA_PIN_HREF
#define PCLK_GPIO_NUM    CONFIG_CAMERA_PIN_PCLK
#endif

/**
 * \brief Callback to invoke each time a photo is taken.
 *
 * \param ctx   Context used in the camera_sequence() function.
 * \param frame Capture photograph. Must be freed when no longer needed by
 *        calling esp_camera_fb_return().
 * \param skip  Number of skipped frames since the last invocation.
 *
 * \return 0 to continue, non-zero to abort the sequence process.
 */
typedef int (*camera_frame_cb)(void *ctx, camera_fb_t *frame, int skip);
typedef void (*camera_finish_cb)(void *ctx);


#ifdef __cplusplus
extern "C" {
#endif

void app_camera_main();

/************************************************************************//**
 * \brief Changes camera pixel format configuration.
 *
 * The function only performs the configuration change if the requested
 * pixel format is not already configured. If no configuration change is
 * performed, the function returns 0. On changes, it returns 1. This allows
 * caller to wait for the sensor to stabilize when the configuration is
 * changed.
 *
 * In addition to the pixel format, the function also changes the image
 * resolution, according to the selected pixel format.
 *
 * \param[in] format Requested pixel format to configure.
 *
 * return 0 if no change is performed, 1 on pixel format change.
 ****************************************************************************/
int camera_set_pixel_format(pixformat_t format);

/************************************************************************//**
 * \brief Starts taking a camera photo sequence.
 *
 * Starts taking photos at regular intervals. Each time a photo is taken,
 * the indicated callback is invoked to process the taken photograph. If
 * the configured photo rate cannot be achieved, frames are skipped and
 * notified in the photo callback.
 *
 * The number of frames taken is the specified in the frames param, or less
 * if frames are skipped or the process is aborted (by the callback returning
 * a non-zero value). A value of 0 frames, causes the process to run
 * indefinitely, until explicitly aborted.
 *
 * \param[in] frames   Number of frames to take. 0 to run forever.
 * \param[in] delay_ms Milliseconds between frames.
 * \param[in] ctx      Context to pass to callback function.
 * \param[in] frame_cb Callback function to invoke with each new frame.
 *
 * return 0 on success, non-zero on error.
 *
 * \warning The camera frame passed in the frame callback, must be returned
 * by the user when no longer needed, by calling esp_camera_fb_return().
 ****************************************************************************/
int camera_sequence(int frames, int delay_ms, void *ctx,
		camera_frame_cb frame_cb, camera_finish_cb finish_cb);

#ifdef __cplusplus
}
#endif

#endif
