/************************************************************************//**
 * /brief Test program for the BATE Telegram client.
 *
 * Creates a simple bot that echoes text and allows taking photographs.
 *
 * \author Jesús Alonso (@doragasu)
 * \date 2019
 ****************************************************************************/
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <nvs_flash.h>
#include <esp_camera.h>
#include <string.h>
#include <bate.h>
#include <ctype.h>

#include "app_wifi.h"
#include "app_camera.h"
#include "util.h"
#include "gif_build.h"

// Shows gif timing related information on the trace
#define _DEBUG_GIF_TIMING

#define IMAGE_WIDTH	320
#define IMAGE_HEIGHT	240

/// Command table
#define COMMAND_TABLE(X_MACRO)		\
	X_MACRO(start,   START)		\
	X_MACRO(photo,   PHOTO)		\
	X_MACRO(gif,     GIF)		\
	X_MACRO(duck,    DUCK)		\
	X_MACRO(gifanim, GIFANIM)

#define X_AS_COMMAND_PARSER_PROTO(l_name, u_name)		\
	static int l_name ## _parse(const char *chat_id,	\
			const struct bate_message *msg,		\
			const struct bate_chat *chat);

COMMAND_TABLE(X_AS_COMMAND_PARSER_PROTO);

#define X_AS_COMMAND_ENUM(l_name, u_name)		\
	CMD_ ## u_name,

enum cmd_id {
	COMMAND_TABLE(X_AS_COMMAND_ENUM)
	CMD_NUM
};

#define X_AS_COMMAND_PARSER_TABLE(l_name, u_name)	\
	l_name ## _parse,

int (* const cmd_parse[])(const char *chat_id, const struct bate_message*,
		const struct bate_chat*) = {
	COMMAND_TABLE(X_AS_COMMAND_PARSER_TABLE)
};

#define X_AS_COMMAND_STRING_TABLE(l_name, u_name)	\
	"/" #l_name,

const char * const cmd_str[CMD_NUM] = {
	COMMAND_TABLE(X_AS_COMMAND_STRING_TABLE)
};

#define X_AS_COMMAND_STRING_LENGTH(l_name, u_name)	\
	sizeof(#l_name),

const unsigned char cmd_len[CMD_NUM] = {
	COMMAND_TABLE(X_AS_COMMAND_STRING_LENGTH)
};

// Memory to pre-allocate initially
#define ALLOC_INITIAL	0x10000

// Used to notify that the gif sequence encoding has finished
#define SEQ_FINISH_BIT		BIT0

/// Holds the data relative to a gif during encoding
struct gif_data {
	char *data;	///< Encoded data
	int length;	///< Length of encoded data
	int allocated;	///< Allocated length for data
};

/// Camera group event used to notify encoding events
static EventGroupHandle_t camera_ev;

/// Gets the bot information
static struct bate_bot_info *bot_get(void)
{
	cJSON *json = NULL;
	struct bate_bot_info *bot = NULL;

	bate_me_get(&json);
	bot = bate_me_parse(json);
	cJSON_Delete(json);

	return bot;
}

/// Sets the camera pixel format
static void pixformat_set(pixformat_t format)
{
	camera_fb_t *photo;
	if (camera_set_pixel_format(format)) {
		// If configuration changed, discard photos for the
		// sensor to stabilize
		LOGI("discarding photographs for the sensor to stabilize");
#ifdef _DEBUG_GIF_TIMING
		long ts = esp_timer_get_time();
#endif
		for (int i = 0; i < 5; i++) {
			photo = esp_camera_fb_get();
			esp_camera_fb_return(photo);
		}
#ifdef _DEBUG_GIF_TIMING
		ts = esp_timer_get_time() - ts;
		LOGI("done, took %ld us", ts);
#endif
	}
}

/// Parses the command requesting to take a photograph
static int photo_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	camera_fb_t *photo;
	int err = -1;

	pixformat_set(PIXFORMAT_JPEG);
	photo = esp_camera_fb_get();
	if (!photo) {
		LOGE("failed to take photo");
		goto out;
	}
	err = bate_file_send(CONFIG_TELEGRAM_TEST_CHAT_ID,
			(const char*)photo->buf, photo->len, "photo.jpg",
			BATE_FILE_PHOTO, BATE_MIME_IMAGE_JPG, NULL);

	esp_camera_fb_return(photo);

out:
	return err;
}

/// Parses the start command
static int start_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	const char * const text = "*Welcome!*\nI am a Telegram bot running on "
		"an ESP32-CAM. I can take photographs and will echo anything I "
		"do not understand, just for testing purposes. Feel free to "
		"write anything  you want. You can browse the bot sources at "
		"[the bate_test repository]"
		"(https://gitlab.com/doragasu/bate_test).";
	// Skip the first command (start)
	const char **keyb = (const char**)&cmd_str[1];
	const unsigned int cols[] = {3, 1};

	return bate_keyboard_reply(chat_id, text, BATE_TEXT_MARKDOWN, keyb,
			2, cols, BATE_KEYB_RESIZE, NULL);
}

static int duck_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	const char text[] = "Cuack!";
	const struct bate_inline_keyboard_button keyb[] = {
		{
			.type = BATE_INLINE_KEYB_TYPE_URL,
			.text = "Search",
			.url = "https://duck.com"
		}, {
			.type = BATE_INLINE_KEYB_TYPE_CALLBACK_DATA,
			.text = "Callback",
			.callback_data = "quack_callback"
		}, {
			.type = BATE_INLINE_KEYB_TYPE_SWITCH_INLINE_QUERY,
			.text = "Switch inline query",
			.switch_inline_query = "quack_query_data"
		}, {
			.type = BATE_INLINE_KEYB_TYPE_SWITCH_INLINE_QUERY_CURRENT_CHAT,
			.text = "Inline query current chat",
			.switch_inline_query_current_chat = "quack_current_chat"
		}
	};
	const unsigned int cols[] = {1, 1, 1, 1};

	return bate_inline_keyboard_reply(chat_id, text, 0, keyb, 4, cols, NULL);
}

/// Called when all the gif frames have been added
void gif_finish_cb(void *context)
{
	struct gif_data *gif = (struct gif_data*)context;

	if (!gif || !gif->data || !gif->length) {
		LOGE("missing data");
		return;
	}

	gif_close();
	// Awake main program thread
	xEventGroupSetBits(camera_ev, SEQ_FINISH_BIT);
}

/// Called when there is new gif compressed data to add
int gif_data_cb(void *context, const char *encoded, int length)
{
	char *new_data;
	int new_length;
	struct gif_data *gif = (struct gif_data*)context;

	if (!gif || !encoded || length <= 0) {
		LOGE("missing data");
		return 0;
	}

	new_length = gif->length + length;
	if (new_length > gif->allocated) {
		gif->allocated <<= 1;
#ifdef _DEBUG_GIF_TIMING
		long ts = esp_timer_get_time();
#endif
		new_data = spi_realloc(gif->data, gif->allocated);
#ifdef _DEBUG_GIF_TIMING
		ts = esp_timer_get_time() - ts;
		LOGI("allocating %d bytes took %ld us", gif->allocated, ts);
#endif
		if (!new_data) {
			LOGE("realloc of %d bytes failed", gif->allocated);
			free(gif->data);
			gif->data = NULL;
			return 0;
		}
		gif->data = new_data;
	}

	memcpy(gif->data + gif->length, encoded, length);
	gif->length = new_length;

	return length;
}

/// Called when a new photo is captured
static int gif_frame_cb(void *ctx, camera_fb_t *frame, int skip)
{
#ifdef _DEBUG_GIF_TIMING
	long ts = esp_timer_get_time();
#endif
	gif_add((const char*)frame->buf, 100, RGB565_INTERLEAVED);
#ifdef _DEBUG_GIF_TIMING
	ts = esp_timer_get_time() - ts;
	LOGI("adding gif took %ld us", ts);
#endif
	esp_camera_fb_return(frame);

	return 0;
}

/// Parses the command requesting to generate a gif
static int gif_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	struct gif_data gif = {};
	int err = -1;

	gif.data = spi_malloc(ALLOC_INITIAL);
	if (!gif.data) {
		LOGE("initial allocation failed");
		goto out;
	}
	gif.allocated = ALLOC_INITIAL;

	gif_generate_test(&gif, gif_data_cb, IMAGE_WIDTH, IMAGE_HEIGHT);

	err = bate_file_send(chat_id, gif.data, gif.length, "test.gif",
			BATE_FILE_ANIMATION, BATE_MIME_IMAGE_GIF, NULL);
	if (err != 200) {
		LOGE("send failed with code: %d", err);
	}
	free(gif.data);

out:
	return err;
}

/// Waits until a gif has been completely encoded
/// \warning Loops forever until gif completed. Should timeout.
static void gif_wait_encoded(void)
{
	while (!(SEQ_FINISH_BIT & xEventGroupWaitBits(camera_ev, SEQ_FINISH_BIT,
					true, true, portMAX_DELAY)));
}

/// Parses the command request to generate an animated gif from camera.
static int gifanim_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	struct gif_data *gif = calloc(1, sizeof(struct gif_data));
	int err = 1;
	const char *status;

	if (!gif) {
		goto out;
	}
	gif->allocated = ALLOC_INITIAL;
	gif->data = spi_malloc(ALLOC_INITIAL);
	if (!gif->data) {
		goto out;
	}
	pixformat_set(PIXFORMAT_RGB565);
	err = gif_init(IMAGE_WIDTH, IMAGE_HEIGHT, gif, gif_data_cb);
	if (err) {
		goto out;
	}
	err = camera_sequence(5, 1000, gif, gif_frame_cb, gif_finish_cb);

	status = err ? "Camera does not work!" : "Gif capture started";
	bate_msg_send(CONFIG_TELEGRAM_TEST_CHAT_ID, status, BATE_TEXT_PLAIN,
			NULL);

	if (err)  {
		goto out;
	}

	gif_wait_encoded();

	LOGI("sending gif %p (%d bytes)", gif->data, gif->length);
	err = bate_file_send(CONFIG_TELEGRAM_TEST_CHAT_ID, gif->data,
			gif->length, "video.gif", BATE_FILE_ANIMATION,
			BATE_MIME_IMAGE_GIF, NULL);
	if (err != 200) {
		LOGE("send failed with code: %d", err);
	}

out:
	free(gif->data);
	free(gif);
	return err;
}

/// Sends the information message for not allowed clients.
static void not_allowed(const char *chat_id, const struct bate_message *msg)
{
	char *warn = NULL;
	struct bate_from *from = NULL;

	bate_msg_send(chat_id, "Nice try, but you are not "
			"allowed to use this bot.", 0, NULL);

	from = bate_from_parse(msg->from);
	if (msg->text) {
		asprintf(&warn, "%s wrote: %s", from->first_name, msg->text);
	} else {
		asprintf(&warn, "%s sent message without text",
				from->first_name);
	}
	bate_msg_send(CONFIG_TELEGRAM_TEST_CHAT_ID, warn,
			BATE_TEXT_PLAIN, NULL);

	bate_from_free(from);
	free(warn);
}

/// Parses the incoming message
static void message_parse(const char *chat_id, const struct bate_message *msg,
		const struct bate_chat *chat)
{
	enum cmd_id cmd;
	size_t len;

	if (msg && msg->text) {
		// Command must start by the command string, and must be
		// followed by string end or by empty space.
		for (cmd = CMD_START; cmd < CMD_NUM; cmd++) {
			len = cmd_len[cmd];
			if (!strncmp(msg->text, cmd_str[cmd], len) &&
					(('\0' == msg->text[len]) ||
					 (isspace((int)msg->text[len])))) {
				cmd_parse[cmd](chat_id, msg, chat);
				break;
			}
		}
		if (CMD_NUM == cmd) {
			// No supported command, echo message
			bate_msg_send(chat_id, msg->text, BATE_TEXT_PLAIN, NULL);
		}
	} else {
		// No text message
		bate_msg_send(chat_id, "No text in message.",
				BATE_TEXT_PLAIN, NULL);
	}
}

/// Parses an incoming JSON message.
static void message_json_parse(const cJSON *msg_json)
{

	char chat_id[32];
	struct bate_message *msg = bate_message_parse(msg_json);
	struct bate_chat *chat = bate_chat_parse(msg->chat);

	snprintf(chat_id, 32, "%ld", chat->id);

	if (strcmp(chat_id, CONFIG_TELEGRAM_TEST_CHAT_ID)) {
		// Message from client not allowed to use the bot
		not_allowed(chat_id, msg);
	} else {
		// Message from the client allowed to use the bot
		message_parse(chat_id, msg, chat);
	}

	bate_message_free(msg);
	bate_chat_free(chat);
}

/// Parses the JSON from an incoming callback query
static void callback_query_json_parse(const cJSON *cbq_json)
{
	struct bate_callback_query *query =
		bate_callback_query_parse(cbq_json);

	bate_callback_query_answer(query->id, "Got it!", false, NULL, 0, NULL);
	bate_callback_query_free(query);
}

/// Handle unsupported updates
static void unsupported_update_parse(void)
{
	LOGI("received unsupported update");
}

/// Process incoming updates
static long process_update(cJSON *update)
{
	long next;
	struct bate_update *up = bate_update_parse(update);
	LOGI("update_id: %ld", up->update_id);

	switch (up->type) {
	case BATE_UPDATE_TYPE_MESSAGE:
		message_json_parse(up->message);
		break;

	case BATE_UPDATE_TYPE_CALLBACK_QUERY:
		callback_query_json_parse(up->callback_query);
		break;

	default:
		unsupported_update_parse();
		break;
	}

	next = up->update_id + 1;
	bate_update_free(up);

	return next;
}

/// Main program task. Handles communications with Telegram bot API
static void telebot_task(void *pvParameters)
{
	struct bate_bot_info *bot = NULL;
	long next_update = 0;
	cJSON *root, *updates, *update;

	app_wifi_wait_connected();
	LOGI("connected to AP, bot start");
	while (!(bot = bot_get())) {
		vTaskDelay(pdMS_TO_TICKS(60000/portTICK_RATE_MS));
	}

	LOGI("connected: id = %ld, first_name = %s, username = %s", bot->id,
			bot->first_name, bot->username);

	bate_msg_send(CONFIG_TELEGRAM_TEST_CHAT_ID,
			"Ready to serve!", 0, NULL);

	LOGI("Entering update loop");
	while ((200 == bate_updates_get(next_update, 300000, &root))) {
		updates = json_get_array(root, "result");
		cJSON_ArrayForEach(update, updates) {
			next_update = process_update(update);
		}
		cJSON_Delete(root);
	}

	LOGW("Bot task has ended");
	bate_me_free(bot);
	vTaskDelete(NULL);
}

/// Main program
void app_main()
{
	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);
	app_wifi_initialise();
	app_camera_main();

	camera_ev = xEventGroupCreate();

	xTaskCreate(&telebot_task, "telebot_task", 8192, NULL, 5, NULL);
}

