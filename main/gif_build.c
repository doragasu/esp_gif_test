#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gif_lib.h>
#include <stdint.h>

#include "gif_build.h"
#include "util.h"

#define COLOR_BPP 8
#define COLOR_MAP_SIZE	(1<<COLOR_BPP)

enum {
	BUF_RED = 0,
	BUF_GREEN,
	BUF_BLUE,
	BUF_NUM
};

struct gif_local_data {
	GifFileType *file;
	out_data_func data_cb;
	int width;
	int height;
	void *user_ctx;
};

static struct gif_local_data gif = {};

static void square_write(GifByteType color[3], GifByteType *buf, int start,
		int sq_width, int sq_height, int width, int height)
{
	int i, j;
	int delta = width * height;
	GifByteType *rgb[BUF_NUM];

	rgb[0] = buf;
	rgb[1] = rgb[0] + delta;
	rgb[2] = rgb[1] + delta;

	for (i = start; sq_height; sq_height--) {
		for (j = i; j < i + sq_width; j++) {
			rgb[0][j] = color[0];
			rgb[1][j] = color[1];
			rgb[2][j] = color[2];
		}
		i += width;
	}
}

static void frame_add(GifFileType *file, GifByteType *frame,
		int width, int height, ColorMapObject* color_map)
{
	int i;
	GifByteType *ptr = frame;

	if (GIF_ERROR == EGifPutImageDesc(file, 0, 0, width, height,
				false, color_map)) {
		LOGE("put image desc failed");
		exit(1);
	}

	for (i = 0; i < height; i++) {
		if (GIF_ERROR == EGifPutLine(file, ptr, width))
			exit(1);
		ptr += width;
	}
}

static void next_frame_add(GifFileType *file, GifByteType *frame, int width,
		int height, ColorMapObject *color_map, uint16_t delay_cs)
{
	char ext_hdr[4];

	ext_hdr[0] = 0x04;
	ext_hdr[1] = delay_cs;
	ext_hdr[2] = delay_cs>>8;
	ext_hdr[3] = 0xFF;

	EGifPutExtension(file, GRAPHICS_EXT_FUNC_CODE, 4, ext_hdr);
	frame_add(file, frame, width, height, color_map);
}

int output_func(GifFileType *file, const GifByteType *data, int length)
{
	struct gif_local_data *local_data = NULL;

	if (!file || !file->UserData) {
		return 0;
	}

	local_data = file->UserData;
	if (!local_data->data_cb) {
		return 0;
	}
	return local_data->data_cb(local_data->user_ctx, (const char*)data, length);
}

static GifFileType *file_open(int width, int height,
		const ColorMapObject *color_map, struct gif_local_data *local_data)
{
	int err;
	GifFileType *file = NULL;

	if (!(file = EGifOpen(local_data, output_func, &err))) {
		LOGE("error %d opening file handle", err);
		goto out;
	}

	if (GIF_ERROR == EGifPutScreenDesc(file, width, height, COLOR_BPP,
				0, color_map)) {
		LOGE("put screen desc failed");
	}

out:
	return file;
}

static int file_close(GifFileType *file)
{
	int err = 0;

	if (GIF_ERROR == EGifCloseFile(file, &err)) {
		LOGE("error %d on file close", err);
	}

	return err;
}

static int image_add(GifFileType *file, GifByteType *buf,
		int delay, int buf_format)
{
	ColorMapObject *color_map;
	int color_map_size = COLOR_MAP_SIZE;
	GifByteType *out_buf;
	int err = true;
	unsigned int num_pixels = gif.width * gif.height;
	GifByteType *rgb[BUF_NUM];


	out_buf = spi_malloc(num_pixels);
	if (!(color_map = GifMakeMapObject(COLOR_MAP_SIZE, NULL))) {
		LOGE("failed to get color map");
		goto out;
	}
	switch (buf_format) {
	case RGB888_BUFS:
		rgb[0] = buf;
		rgb[1] = rgb[0] + num_pixels;
		rgb[2] = rgb[1] + num_pixels;
		err = GifQuantizeBuffer(gif.width, gif.height, &color_map_size,
			rgb[BUF_RED], rgb[BUF_GREEN], rgb[BUF_BLUE],
			out_buf, color_map->Colors);
		break;

	case RGB888_INTERLEAVED:
		err = GifQuantizeRGB888(gif.width, gif.height, &color_map_size,
			buf, out_buf, color_map->Colors);
		break;

	case RGB565_INTERLEAVED:
		err = GifQuantizeRGB565(gif.width, gif.height, &color_map_size,
			buf, out_buf, color_map->Colors);
		break;

	default:
		err = GIF_ERROR;
	}
	if (GIF_ERROR == err) {
		LOGE("quantization failed");
		err = true;
		goto out;
	}
	next_frame_add(file, out_buf, gif.width, gif.height, color_map, delay);
	err = false;

out:
	free(out_buf);
	return err;
}

int gif_init(int width, int height, void *user_data, out_data_func data_cb)
{
	gif.width = width;
	gif.height = height;
	gif.data_cb = data_cb;
	gif.user_ctx = user_data;

	gif.file = file_open(width, height, NULL, &gif);

	return !gif.file;
}

int gif_add(const char *buf, int delay_cs, enum gif_source_format format)
{
	int err = image_add(gif.file, (GifByteType*)buf, delay_cs, format);

	return err;
}

int gif_close(void)
{
	return file_close(gif.file);
}

int gif_generate_test(void *user_data, out_data_func data_cb,
		int width, int height)
{
	GifByteType *in_buf;

	in_buf = spi_malloc(width * height * 3);
	gif_init(width, height, user_data, data_cb);

	memset(in_buf, 0, width * height * 3);
	square_write((GifByteType[3]){255, 0, 0}, in_buf, 0, 80, 80,
			width, height);
	gif_add((const char*)in_buf, 100, RGB888_BUFS);

	memset(in_buf, 0, width * height * 3);
	square_write((GifByteType[3]){0, 255, 0}, in_buf, 80 * width + 80,
			80, 80, width, height);
	gif_add((const char*)in_buf, 100, RGB888_BUFS);

	memset(in_buf, 0, width * height * 3);
	square_write((GifByteType[3]){0, 0, 255}, in_buf, 160 * width + 160,
			80, 80, width, height);
	gif_add((const char*)in_buf, 100, RGB888_BUFS);

	gif_close();
	free(in_buf);

	return 0;
}

