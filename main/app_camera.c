/* ESPRESSIF MIT License
 * 
 * Copyright (c) 2018 <ESPRESSIF SYSTEMS (SHANGHAI) PTE LTD>
 * 
 * Permission is hereby granted for use on all ESPRESSIF SYSTEMS products, in which case,
 * it is free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 * to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <esp_log.h>
#include <esp_timer.h>
#include "app_camera.h"
#include "sdkconfig.h"
#include "util.h"

/// Data required to handle a photo sequence
struct sequence_data {
	int64_t time_last;
	int frames;
	int interval_us;
	esp_timer_handle_t timer;
	camera_frame_cb frame_cb;
	camera_finish_cb finish_cb;
	void *ctx;
};

static const char *TAG = "app_camera";

void app_camera_main ()
{
#if CONFIG_CAMERA_MODEL_ESP_EYE
    /* IO13, IO14 is designed for JTAG by default,
     * to use it as generalized input,
     * firstly declair it as pullup input */
    gpio_config_t conf;
    conf.mode = GPIO_MODE_INPUT;
    conf.pull_up_en = GPIO_PULLUP_ENABLE;
    conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    conf.intr_type = GPIO_INTR_DISABLE;
    conf.pin_bit_mask = 1LL << 13;
    gpio_config(&conf);
    conf.pin_bit_mask = 1LL << 14;
    gpio_config(&conf);
#endif

    camera_config_t config;
    config.ledc_channel = LEDC_CHANNEL_0;
    config.ledc_timer = LEDC_TIMER_0;
    config.pin_d0 = Y2_GPIO_NUM;
    config.pin_d1 = Y3_GPIO_NUM;
    config.pin_d2 = Y4_GPIO_NUM;
    config.pin_d3 = Y5_GPIO_NUM;
    config.pin_d4 = Y6_GPIO_NUM;
    config.pin_d5 = Y7_GPIO_NUM;
    config.pin_d6 = Y8_GPIO_NUM;
    config.pin_d7 = Y9_GPIO_NUM;
    config.pin_xclk = XCLK_GPIO_NUM;
    config.pin_pclk = PCLK_GPIO_NUM;
    config.pin_vsync = VSYNC_GPIO_NUM;
    config.pin_href = HREF_GPIO_NUM;
    config.pin_sscb_sda = SIOD_GPIO_NUM;
    config.pin_sscb_scl = SIOC_GPIO_NUM;
    config.pin_pwdn = PWDN_GPIO_NUM;
    config.pin_reset = RESET_GPIO_NUM;
    config.xclk_freq_hz = 20000000;
    config.pixel_format = PIXFORMAT_RGB565;
    config.frame_size = FRAMESIZE_QVGA;
    config.jpeg_quality = 10;
    config.fb_count = 1;

    // camera init
    esp_err_t err = esp_camera_init(&config);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Camera init failed with error 0x%x", err);
        return;
    }

    sensor_t * s = esp_camera_sensor_get();
    //initial sensors are flipped vertically and colors are a bit saturated
    if (s->id.PID == OV3660_PID) {
//        s->set_vflip(s, 1);//flip it back
        s->set_brightness(s, 1);//up the blightness just a bit
        s->set_saturation(s, -2);//lower the saturation
    }
}

int camera_set_pixel_format(pixformat_t format)
{
	sensor_t *sensor = esp_camera_sensor_get();

	if (sensor->pixformat == format) {
		// No need to change config
		return 0;
	}

	sensor->set_pixformat(sensor, format);
	if (PIXFORMAT_JPEG == format) {
		// Big resolution only for JPEG
		sensor->set_framesize(sensor, FRAMESIZE_UXGA);
	} else {
		sensor->set_framesize(sensor, FRAMESIZE_QVGA);
	}

	return 1;
}

static void timer_cb(void *arg)
{
	camera_fb_t *photo;
	int64_t time = esp_timer_get_time();
	struct sequence_data *seq = (struct sequence_data*)arg;
	int skip;
	bool end;

	photo = esp_camera_fb_get();
	if (!photo) {
		LOGE("failed to take photo");
		// TODO should we abort the process?
		return;
	}
	skip = ((time - seq->time_last)/seq->interval_us) - 1;
	seq->time_last = time;

	end = seq->frame_cb(seq->ctx, photo, skip);
	if (seq->frames) {
//		seq->frames -= (1 + skip);
		seq->frames--;
		if (seq->frames <= 0) {
			end = true;
		}
	}
	if (end) {
		esp_timer_stop(seq->timer);
		esp_timer_delete(seq->timer);
		if (seq->finish_cb) {
			seq->finish_cb(seq->ctx);
		}
		free(seq);
	}
}

int camera_sequence(int frames, int interval_ms, void *ctx,
		camera_frame_cb frame_cb, camera_finish_cb finish_cb)
{
	struct sequence_data *seq = calloc(1, sizeof(struct sequence_data));

	seq->frames = frames;
	seq->interval_us = interval_ms * 1000;
	seq->frame_cb = frame_cb;
	seq->finish_cb = finish_cb;
	seq->ctx = ctx;

	const esp_timer_create_args_t timer_args = {
		.callback = timer_cb,
		.name = "frame_timer",
		.arg = seq
	};

	if (!frame_cb) {
		LOGE("missing frame callback");
		return 1;
	}

	seq->time_last = esp_timer_get_time();
	if (esp_timer_create(&timer_args, &seq->timer) ||
			esp_timer_start_periodic(seq->timer,
				interval_ms * 1000)) {
		LOGE("frame timer error");
		return 1;
	}
	return 0;
}

