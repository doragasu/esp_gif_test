#ifndef _UTIL_H_
#define _UTIL_H_

#include <esp_log.h>
#include <esp_heap_caps.h>

// Allocate RAM in SPI memory. Use free() to dealloc
#define spi_malloc(len) heap_caps_malloc(len, MALLOC_CAP_SPIRAM)

// Allocate and zero RAM in SPI memory. Use free to dealloc
#define spi_calloc(len) heap_caps_calloc(len, MALLOC_CAP_SPIRAM)

// Reallocate RAM in SPI memory
#define spi_realloc(ptr, len) heap_caps_realloc(ptr, len, MALLOC_CAP_SPIRAM)

// Map ESP_LOGE() macros to something easier to use
#define LOGE(...) ESP_LOGE(__func__, __VA_ARGS__)
#define LOGD(...) ESP_LOGD(__func__, __VA_ARGS__)
#define LOGI(...) ESP_LOGI(__func__, __VA_ARGS__)
#define LOGW(...) ESP_LOGW(__func__, __VA_ARGS__)

#endif /*_UTIL_H_*/

